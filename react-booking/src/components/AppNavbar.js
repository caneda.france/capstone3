import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return (

		<Navbar style={{backgroundColor:"white"}} expand="lg">
		    <Container fluid>
		    {(user.isAdmin)
		    ?
		        <Navbar.Brand as={Link} to="/adminHome"><img src="./tako1.jpg" alt="bug" height={50}/></Navbar.Brand>
		       :

		        <Navbar.Brand as={Link} to="/"><img src="./tako1.jpg" alt="bug" height={50}/></Navbar.Brand>
		    }
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="me-auto">
		          	{
		          	 (user.isAdmin)
    	            	
    	            	?
    	       			<>

    	       			<Nav.Link as={ NavLink } to="/adminHome" end>Home</Nav.Link>
    	            	<Nav.Link as={ NavLink } to="/adminDashboard" end>Admin Dashboard</Nav.Link>
    	       			

    	            	
    	            	</>
    	            	:
    	            	<>
    	            	
    	            	<Nav.Link as={NavLink} to="/">Home</Nav.Link>
    	            	<Nav.Link as={NavLink} to="/products">Menu </Nav.Link>
    	            	<Nav.Link as={NavLink} to="/faq">FAQS</Nav.Link>
    	            	</>
    	            }

    	          
    	            

		          		
			           


			            {(user.id !== null)
			            	?
			            	<>
			            			
			            	    
			            	     	
			            			<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>

			            			</>
			            	:
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
			            		
			            	</Fragment>

			            
			            }
			          
			            
			           
			            
			        
			            
		          	</Nav>
		        </Navbar.Collapse>
		     </Container>
		</Navbar>
	)
}