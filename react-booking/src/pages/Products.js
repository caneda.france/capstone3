import { Fragment, useEffect, useState } from 'react';
 //import productsData from '../data/productsData';
import ProductsCard from '../components/ProductsCard';


export default function Products() {
	// Checks to see if the mock data was captured
	//console.log("Hello");

	// State that will be used to store the products retrieved from the database
	const [products, setProducts] = useState([]);

	// "map" method loops through the individual productsin our mock database and returns a Productscard component for each products	// Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
	// Everytime the map method loops through the data, it creates a "Productscard" component and then passes the current element in our productsData array using the productsrop
	 /*const products = productsData.map(products=> {
	 	return (
			<ProductsCard key={products_id} productsProp={products/>
	 	)
	})*/

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(products=> {
				return(
					<ProductsCard key={products._id} productsProp={products}/>
				)
			}))
		})

	}, [])

	return (
		<Fragment>
			
			
		 	<h1 style={{textAlign:"center"}}>Menu</h1>
		 	{products}
		 </Fragment>
		)
	
}
