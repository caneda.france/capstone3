import { Fragment } from 'react';
import Banner from '../components/Banner';
import Footer from '../components/Footer';


export default function AdminHome() {

	const data = {
	    title: "Welcome back, admin!",
	    content: "Make changes and keep the menu up-to-date. Click the button to make changes now.",
	    destination: "/adminDashboard",
	    label: "Make Changes"
	}
	
	return (
		<Fragment>
			<Banner data={data}/>
			<Footer/>
		</Fragment>
	)
}