import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductsView() {

	const navigate = useNavigate();

	// To be able to obtain user ID so we can enroll a user
	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	
	const  [quantity, setQuantity] = useState(0);

	const makeOrder = (productId, quantity) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Added to your cart!",
					icon: "success",
					text: "You have successfully added this product in your cart."
				})

				navigate("/products");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
		})

	}, [productId])

	return(
		<Container>
			<Row>
			<Col md={3}>
			</Col>
				<Col md={6}>
					<Card>
					    <Card.Body className="text-center">
					        <Card.Title style={{paddingBottom:"1.5em"}}>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle style={{paddingBottom:"1.5em"}}>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					      	<Form onSubmit={(e) => makeOrder(e)}> 
						    	<Form.Group controlId="quantity">
						        <Form.Label>Quantity</Form.Label>
						        <Form.Control 
						            type="number" 
						            placeholder="Enter Quantity"
						            value={quantity} 
						            onChange={e => setQuantity(e.target.value)}
						            required
						        />
						    	</Form.Group>
						    </Form>
						  	 
					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => makeOrder (productId, quantity)}>Order</Button>
					        	:
					        	<Button variant="danger" as={Link} to="/login">Log in to Order</Button>
					        }
					    </Card.Body>
					</Card>
				</Col>
				<Col md={3}>
			</Col>
			</Row>
		</Container>
	)
}