import { Fragment, useEffect, useState } from 'react';
 //import productsData from '../data/productsData';
import InactiveProductsCard from '../components/InactiveProductsCard';


export default function InactiveProducts() {
	
	const [inactiveProducts, setInactiveProducts] = useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/inactive`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setInactiveProducts(data.map(inactiveProducts=> {
				return(
					<InactiveProductsCard key={inactiveProducts._id} inactiveProductsProp={inactiveProducts}/>
				)
			}))
		})

	}, [])

	return (
		<Fragment>
			
			
		 	<h1>Menu</h1>
		 	{inactiveProducts}
		 </Fragment>
		)
	
}
